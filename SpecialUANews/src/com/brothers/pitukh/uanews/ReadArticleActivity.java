package com.brothers.pitukh.uanews;

import android.annotation.TargetApi;
import android.graphics.Point;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.util.Log;
import android.view.Display;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.TextView;

import com.actionbarsherlock.app.SherlockActivity;
import com.actionbarsherlock.view.MenuItem;
import com.brothers.pitukh.uanews.db.ArticleDataSource;
import com.brothers.pitukh.uanews.model.Article;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.ViewById;

@EActivity(R.layout.activity_read_article)
public class ReadArticleActivity extends SherlockActivity {

	@ViewById(value = R.id.articleBody)
	WebView webview;
	@ViewById(value = R.id.articleTitle)
	TextView articleTitleTextView;
	@ViewById(value = R.id.articleNewspaper)
	TextView articleNewspaperTextView;
	
	public static int displayWidth;
	public static int displayHeight;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(com.actionbarsherlock.R.style.Theme_Sherlock);
		super.onCreate(savedInstanceState);
		displayWidth = getWidth();
		displayHeight = getHeight();
	}
	
	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	int getWidth() {
		Display display = getWindowManager().getDefaultDisplay();
		if (android.os.Build.VERSION.SDK_INT >= 13) {
			Point size = new Point();
			display.getSize(size);
			return size.x;
		} else {
			return display.getWidth();
		}
	}

	@TargetApi(Build.VERSION_CODES.HONEYCOMB_MR2)
	int getHeight() {
		Display display = getWindowManager().getDefaultDisplay();
		if (android.os.Build.VERSION.SDK_INT >= 13) {
			Point size = new Point();
			display.getSize(size);
			return size.y;
		} else {
			return display.getHeight();
		}
	}

	ArticleDataSource datasource;

	final String HEADER_TEMPLATE = "<div style=\"background-color:rgb(168,223,244)\"><h2><center>#name</center></h2><h3>#article</h3></div>";

	@AfterViews
	public void after() {
		webview.setHorizontalScrollbarOverlay(true);
		webview.setHorizontalScrollBarEnabled(false);
		webview.setScrollBarStyle(WebView.SCROLLBARS_INSIDE_OVERLAY);
		
		datasource = new ArticleDataSource(this);
		datasource.open();
		long idInDB = getIntent().getLongExtra("articleIdentifier", -1L);
		if (idInDB != -1L) {
			Article tempArticle = datasource.getArticleByID(idInDB);
			WebSettings settings = webview.getSettings();
			settings.setDefaultTextEncodingName("utf-8");
			String header = HEADER_TEMPLATE;
			header = header.replace("#name", tempArticle.getTitle());
			header = header.replace("#article", tempArticle.getDate().toString());
			String text = header + tempArticle.getFullText();
			webview.loadDataWithBaseURL(null, tempArticle.getFullText(), "text/html", "utf-8", null);
			// webview.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
			articleTitleTextView.setText(tempArticle.getTitle());
			articleNewspaperTextView.setText("��������� ������");
			//webview.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
			//webview.getSettings().setLoadWithOverviewMode(true);
			webview.getSettings().setUseWideViewPort(true);
			// webview.loadData(tempArticle.getFullText(), "text/html; charset=UTF-8", "UTF-8");
		} else {
			Log.v("MYTAG", "idInDB is -1");
		}
		getSupportActionBar().setDisplayHomeAsUpEnabled(true);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			// This ID represents the Home or Up button. In the case of this
			// activity, the Up button is shown. Use NavUtils to allow users
			// to navigate up one level in the application structure. For
			// more details, see the Navigation pattern on Android Design:
			//
			// http://developer.android.com/design/patterns/navigation.html#up-vs-back
			//
			NavUtils.navigateUpFromSameTask(this);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onPause() {
		datasource.close();
		super.onPause();
	}

	@Override
	protected void onResume() {
		datasource.open();
		super.onResume();
	}
}
