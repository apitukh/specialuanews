package com.brothers.pitukh.uanews.parsers;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import com.brothers.pitukh.uanews.ReadArticleActivity;

public class ParserUtility {

	static String cleanPage(String html) {
		return cleanPage(Jsoup.parse(html));
	}
	
	static String cleanPage(Document doc) {
		if (!doc.head().hasText()) {
			doc.prepend("<html><head></head><body>");
			doc.append("</body></html>");
		}
		doc.head().append("<meta name=\"viewport\" content=\"width=WIDTH_OF_YOUR_PAGE, target-densitydpi=device-dpi, user-scalable=no\" />");
		for (Element el : doc.getElementsByTag("img")) {
			int width = -1;
			try {
				width = Integer.parseInt(el.attr("width"));
			} catch (Exception e) {
			}
			if (width > ReadArticleActivity.displayWidth) {
				el.removeAttr("width");
				el.removeAttr("height");
				el.attr("width", "" + ReadArticleActivity.displayWidth);
			}
		}
		return doc.outerHtml();		
	}
	
}
