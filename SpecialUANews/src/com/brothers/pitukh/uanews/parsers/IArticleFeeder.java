package com.brothers.pitukh.uanews.parsers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import com.brothers.pitukh.uanews.model.Article;

public interface IArticleFeeder {

	public List<Article> getLatestArticles() throws MalformedURLException, IOException;
	
}
