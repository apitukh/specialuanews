package com.brothers.pitukh.uanews.parsers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import com.brothers.pitukh.uanews.model.Article;

public class UPPublicationsRSSParser implements IArticleFeeder {

	public List<Article> getLatestArticles() throws MalformedURLException, IOException {
		return UPUniversalRSSParser.getArticlesFromContent(new URL("http://www.pravda.com.ua/rss/view_pubs/").openStream());
	}

}
