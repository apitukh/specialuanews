package com.brothers.pitukh.uanews.parsers;

import java.io.InputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.jsoup.Jsoup;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.brothers.pitukh.uanews.model.Article;

public class UPUniversalRSSParser {

	public static List<Article> getArticlesFromContent(InputStream stream) {
		List<Article> articles = new ArrayList<Article>();
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		Document doc = null;
		try {
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			doc = dBuilder.parse(stream);
		} catch (Exception e) {
			System.err.println(e);
			return null;
		}

		// optional, but recommended
		// read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
		doc.getDocumentElement().normalize();

		NodeList nList = doc.getElementsByTagName("item");

		for (int temp = 0; temp < nList.getLength(); temp++) {
			Node nNode = nList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				try {
					Article article = new Article();
					article.setTitle(eElement.getElementsByTagName("title").item(0).getTextContent());
					article.setUrl(new URL(eElement.getElementsByTagName("link").item(0).getTextContent()));
					String authorBad = eElement.getElementsByTagName("author").item(0).getTextContent();
					String authorGood = authorBad.substring(authorBad.indexOf('(') + 1);
					if (authorGood.contains(",")) {
						authorGood = authorGood.substring(0, authorGood.lastIndexOf(','));
					} else if (authorGood.contains(")")) {
						authorGood = authorGood.substring(0, authorGood.lastIndexOf(')'));
					}
					article.setAuthor(authorGood);
					// Date
					DateFormat df = new SimpleDateFormat("EEE, dd MMM yyyy kk:mm:ss", Locale.ENGLISH);
					Date result = df.parse(eElement.getElementsByTagName("pubDate").item(0).getTextContent());
					article.setDate(result);
					// Full text
					NodeList fullTextElements = eElement.getElementsByTagName("fulltext");
					if (fullTextElements != null && fullTextElements.item(0) != null) {
						org.jsoup.nodes.Document doc2 = Jsoup.connect(article.getUrl().toExternalForm()).get();
						String core = doc2.select(".text").first().html();
						article.setFullText(ParserUtility.cleanPage(core));
					}
					try {
						article.setImageUrl(new URL(((Element) eElement.getElementsByTagName("enclosure").item(0)).getAttribute("url")));
					} catch (NullPointerException e) {
					}
					article.setShortDescription(eElement.getElementsByTagName("description").item(0).getTextContent());
					articles.add(article);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

		return articles;
	}

}
