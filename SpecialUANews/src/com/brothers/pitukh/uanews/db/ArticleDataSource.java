package com.brothers.pitukh.uanews.db;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

import com.brothers.pitukh.uanews.model.Article;
import com.brothers.pitukh.uanews.model.NewspaperChapter;

/***
 * Provide convenient interface for interaction with Database (MySQLiteHelper): add, get and delete (TODO: we have to delete articles after some time
 * has passed) articles from database
 * 
 * @author APitukh
 * 
 */
public class ArticleDataSource {

	// Database fields
	private SQLiteDatabase database;
	private MySQLiteHelper dbHelper;
	private String[] allColumns = { MySQLiteHelper.COLUMN_ID, MySQLiteHelper.COLUMN_NEWSPAPER_TYPE, MySQLiteHelper.COLUMN_TITLE,
			MySQLiteHelper.COLUMN_AUTHOR, MySQLiteHelper.COLUMN_URL, MySQLiteHelper.COLUMN_DATE, MySQLiteHelper.COLUMN_FULL_TEXT };

	public ArticleDataSource(Context context) {
		dbHelper = new MySQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	/*
	public void addArticles(List<Article> newArticles, NewspaperChapter currentNewspaper) {
		for (Article art : newArticles) {
			addArticle(art, currentNewspaper);
		}
	}*/

	public List<Article> getSavedArticles(NewspaperChapter currentNewspaper) {
		List<Article> result = new ArrayList<Article>();
		// TODO: add Date column and order by it
		Cursor cursor = database.query(MySQLiteHelper.TABLE_ARTICLES, allColumns, MySQLiteHelper.COLUMN_NEWSPAPER_TYPE + "=?", new String[] { ""
				+ currentNewspaper.ordinal() }, null, null, MySQLiteHelper.COLUMN_DATE + " DESC");
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			try {
				Article article = cursorToArticle(cursor);
				result.add(article);
			} catch (ParseException e) {
				e.printStackTrace();
			}
			cursor.moveToNext();
		}
		return result;
	}

	private Article cursorToArticle(Cursor cursor) throws ParseException {
		Article article = new Article();
		article.setId(cursor.getLong(0));
		// TODO: what about getInt(1)
		article.setTitle(cursor.getString(2));
		article.setAuthor(cursor.getString(3));
		String date = cursor.getString(5);
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		article.setDate(df.parse(date));
		article.setFullText(cursor.getString(6));
		try {
			article.setUrl(new URL(cursor.getString(4)));
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		return article;
	}

	public void clear() {
		try {
			database.delete(MySQLiteHelper.TABLE_ARTICLES, null, null);
		} catch (SQLiteException e) {
			Log.w("MYTAG", Log.getStackTraceString(e));
		}
	}

	public Article getArticleByID(long idInDB) {
		Cursor cursor = database.query(MySQLiteHelper.TABLE_ARTICLES, allColumns, MySQLiteHelper.COLUMN_ID + "=?", new String[] { ""
				+ idInDB }, null, null, null);
		cursor.moveToFirst();
		if (!cursor.isAfterLast()) {
			try {
				return cursorToArticle(cursor);
			} catch (ParseException e) {
				e.printStackTrace();
				return null;
			}
		}
		return null;
	}

	public boolean addArticle(Article art, NewspaperChapter currentNewspaper) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COLUMN_NEWSPAPER_TYPE, currentNewspaper.ordinal());
		values.put(MySQLiteHelper.COLUMN_TITLE, art.getTitle());
		values.put(MySQLiteHelper.COLUMN_AUTHOR, art.getAuthor());
		values.put(MySQLiteHelper.COLUMN_URL, art.getUrl().toExternalForm());
		if (art.getDate() != null) {
			DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			values.put(MySQLiteHelper.COLUMN_DATE, df.format(art.getDate()));
		}
		//if (art.getFullText()!=null) {
			values.put(MySQLiteHelper.COLUMN_FULL_TEXT, art.getFullText());
		//}
		try {
			long articleID = database.insertOrThrow(MySQLiteHelper.TABLE_ARTICLES, null, values);
			art.setId(articleID);
		} catch (SQLiteConstraintException e) {
			Log.v("MYTAG", "Article: " + art.getTitle() + " � already exists in database");
			return false;
		}
		return true;
	}

}