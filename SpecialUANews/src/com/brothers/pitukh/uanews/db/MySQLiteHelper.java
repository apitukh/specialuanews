package com.brothers.pitukh.uanews.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/***
 * This class is responsible for database creation and upgrading (that is, dropping and recreation).
 * 
 * @author APitukh
 * 
 */
public class MySQLiteHelper extends SQLiteOpenHelper {

	public static final String TABLE_ARTICLES = "articles";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_NEWSPAPER_TYPE = "_newspaper_type";
	public static final String COLUMN_TITLE = "title";
	public static final String COLUMN_AUTHOR = "author";
	public static final String COLUMN_URL = "url";
	public static final String COLUMN_DATE = "articledate";
	public static final String COLUMN_FULL_TEXT = "fulltext";

	private static final String DATABASE_NAME = "articles.db";
	private static final int DATABASE_VERSION = 7;

	// Database creation sql statement
	private static final String DATABASE_CREATE = "create table " + TABLE_ARTICLES + "(" + COLUMN_ID + " integer primary key autoincrement, "
			+ COLUMN_NEWSPAPER_TYPE + " integer not null, " + COLUMN_TITLE + " text not null, " + COLUMN_AUTHOR + " text not null, " + COLUMN_URL
			+ " text not null, " + COLUMN_DATE + " datetime not null, " + COLUMN_FULL_TEXT + " text, UNIQUE (" + COLUMN_NEWSPAPER_TYPE + "," + COLUMN_URL + "));";

	public MySQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(DATABASE_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(MySQLiteHelper.class.getName(), "Upgrading database from version " + oldVersion + " to " + newVersion
				+ ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_ARTICLES);
		onCreate(db);
	}

}
