package com.brothers.pitukh.uanews;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.ActionBar.OnNavigationListener;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.brothers.pitukh.uanews.db.ArticleDataSource;
import com.brothers.pitukh.uanews.model.NewspaperChapter;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.EActivity;
import com.googlecode.androidannotations.annotations.ViewById;
import com.viewpagerindicator.TabPageIndicator;

@EActivity(R.layout.simple_tabs)
public class MainActivity extends SherlockFragmentActivity implements OnNavigationListener {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		setTheme(com.actionbarsherlock.R.style.Theme_Sherlock);
		super.onCreate(savedInstanceState);
	}

	@ViewById
	ListView listView1;

	final Activity activity = this;
	ArticleDataSource datasource;
	ArrayAdapter<CharSequence> actionBarArrayAdapter;
	NewspaperChapter currentNewspaper = NewspaperChapter.UPPublications;

	class GoogleMusicAdapter extends FragmentPagerAdapter {
		public GoogleMusicAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			NewspaperChapter newspaper = (NewspaperChapter) currentNewspaper.getParent().getChildren().toArray()[position];
			return ArticleListFragment_.newInstance(newspaper.ordinal());
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return ((NewspaperChapter) currentNewspaper.getParent().getChildren().toArray()[position]).getFullName();
		}

		@Override
		public int getCount() {
			return currentNewspaper.getParent().getChildren().size();
		}
	}

	@AfterViews
	void afterViews() {
		datasource = new ArticleDataSource(this);
		datasource.open();

		FragmentPagerAdapter adapter = new GoogleMusicAdapter(getSupportFragmentManager());
		ViewPager pager = (ViewPager) findViewById(R.id.pager);
		pager.setAdapter(adapter);
		TabPageIndicator indicator = (TabPageIndicator) findViewById(R.id.indicator);
		indicator.setViewPager(pager);
		indicator.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
			public void onPageSelected(int position) {
				currentNewspaper = (NewspaperChapter) currentNewspaper.getParent().getChildren().toArray()[position];
			}
		});
		if (pager.getAdapter().getCount() == 1) {
			indicator.removeAllViews();
		}
		configureActionBar();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getSupportMenuInflater();
		menu.add("Refresh").setIcon(R.drawable.navigation_refresh_holodark).setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
		return true;
	}

	private void configureActionBar() {
		Context context = getSupportActionBar().getThemedContext();
		actionBarArrayAdapter = ArrayAdapter.createFromResource(context, R.array.locations, (R.layout.header_list_item));
		getSupportActionBar().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		getSupportActionBar().setListNavigationCallbacks(actionBarArrayAdapter, this);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
	}

	@Override
	public boolean onOptionsItemSelected(com.actionbarsherlock.view.MenuItem item) {
		if (item.getTitle().equals("Refresh")) {
			Toast.makeText(this, "SOMETEXTT", Toast.LENGTH_SHORT).show();
			// Get last articles:
			((ArticleListFragment_) getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.pager + ":0")).getLastArticles();
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		CharSequence item = actionBarArrayAdapter.getItem(itemPosition);
		if (item.equals("��������� ������")) {
			currentNewspaper = NewspaperChapter.UPPublications;
		} else if (item.equals("����")) {
			currentNewspaper = NewspaperChapter.UPMainNews;
		} else {
		}
		Toast.makeText(this, item, Toast.LENGTH_SHORT).show();
		return true;
	}

	@Override
	protected void onPause() {
		datasource.close();
		super.onPause();
	}

	@Override
	protected void onResume() {
		datasource.open();
		super.onResume();
	}
}
