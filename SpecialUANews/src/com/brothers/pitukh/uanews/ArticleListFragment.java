package com.brothers.pitukh.uanews;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.brothers.pitukh.uanews.db.ArticleDataSource;
import com.brothers.pitukh.uanews.model.Article;
import com.brothers.pitukh.uanews.model.NewspaperChapter;
import com.googlecode.androidannotations.annotations.AfterViews;
import com.googlecode.androidannotations.annotations.Background;
import com.googlecode.androidannotations.annotations.EFragment;
import com.googlecode.androidannotations.annotations.UiThread;
import com.googlecode.androidannotations.annotations.ViewById;

@EFragment
public class ArticleListFragment extends Fragment {
	static Map<Integer, ArticleListFragment> savedFragments = new HashMap<Integer, ArticleListFragment>();

	static String KEY_CONTENT = "newspaper-number";

	public static ArticleListFragment newInstance(int content) {
		Log.v("MYTAG", "INPUT");
		if (savedFragments.containsKey(content)) {
			return savedFragments.get(content);
		}
		ArticleListFragment fragment = new ArticleListFragment_();
		fragment.newspaperNumber = content;
		savedFragments.put(content, fragment);
		return fragment;
	}

	@ViewById
	ListView listView1;
	MySimpleArrayAdapter listViewAdapter;
	ArticleDataSource datasource;
	NewspaperChapter currentNewspaper;

	int newspaperNumber;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
		// mContent = savedInstanceState.getString(KEY_CONTENT);
		// }
	}

	// See here: http://developer.android.com/guide/components/fragments.html
	// For Fragment lifecycle! If I place this code in onCreate, it'll be deleted after resuming
	@Override
	public void onActivityCreated(Bundle savedInstanceState) {
		super.onActivityCreated(savedInstanceState);
		datasource = ((MainActivity_) getActivity()).datasource;
		if ((savedInstanceState != null) && savedInstanceState.containsKey(KEY_CONTENT)) {
			newspaperNumber = savedInstanceState.getInt(KEY_CONTENT);
		}
		currentNewspaper = (NewspaperChapter) ((MainActivity_) getActivity()).currentNewspaper.getParent().getChildren().toArray()[newspaperNumber];
		getLastArticles();
	};

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.v("MYTAG", "INPUT3");
		RelativeLayout view = (RelativeLayout) inflater.inflate(R.layout.activity_main, container, false);
		return view;
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putInt(KEY_CONTENT, newspaperNumber);
	}

	@Background
	void getLastArticles() {
		try {
			if (currentNewspaper == null) {
				Log.e("MYTAG", "WHAT A MISTAKE!");
			}
			// TODO: remove this. We already open it in MainActivity, but they sometimes overlaps
			if (datasource != null) {
				datasource.open();
			} else {
				Log.v("MYTAG", "Datasource is somehow null!");
			}
			List<Article> savedArticles = datasource.getSavedArticles(currentNewspaper);
			addArticlesToView(savedArticles);
			for (Article newArticle: currentNewspaper.getLatest()) {
				if (datasource.addArticle(newArticle, currentNewspaper)) {
					addArticleToView(newArticle);
				}
			}
		} catch (MalformedURLException e) {
			Log.v("MYTAG", Log.getStackTraceString(e));
		} catch (IOException e) {
			Log.v("MYTAG", Log.getStackTraceString(e));
		}
	}

	@UiThread
	void addArticleToView(Article article) {
		List<Article> initList = new ArrayList<Article>();
		initList.add(article);
		addArticlesToView(initList);
	}

	@UiThread
	void addArticlesToView(List<Article> articles) {
		if (listViewAdapter == null) {
			listViewAdapter = new MySimpleArrayAdapter(getActivity(), articles, MySimpleArrayAdapter.getIds(articles));
			listView1.setAdapter(listViewAdapter);
		} else {
			listView1.setAdapter(listViewAdapter);
			boolean changed = false;
			for (Article article : articles) {
				if (listViewAdapter.addArticle(article)) {
					listViewAdapter.add(article.getId());
					changed = true;
				}
			}
			if (changed) {
				listViewAdapter.notifyDataSetChanged();
			}
		}
	}

	@AfterViews
	public void after() {
		listView1.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				Object o = listView1.getItemAtPosition(position);
				Intent i = new Intent(getActivity(), ReadArticleActivity_.class);
				Log.v("MYTAG", listViewAdapter.getArticles().get(o).getUrl().toExternalForm());
				i.putExtra("articleIdentifier", listViewAdapter.getArticles().get(o).getId());
				startActivity(i);
			}
		});
	}

}
