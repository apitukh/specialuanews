package com.brothers.pitukh.uanews.model;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import com.brothers.pitukh.uanews.parsers.IArticleFeeder;
import com.brothers.pitukh.uanews.parsers.UPMainNewsRSSParser;
import com.brothers.pitukh.uanews.parsers.UPPublicationsRSSParser;

public enum NewspaperChapter {

	UPPublications("���������", UPPublicationsRSSParser.class) {
		@Override
		public Newspaper getParent() {
			return Newspaper.UP;
		}
	},
	UPMainNews("������� ������", UPMainNewsRSSParser.class) {
		@Override
		public Newspaper getParent() {
			return Newspaper.UP;
		}
	};

	String fullName;
	Newspaper newspaper;
	IArticleFeeder articleFeeder;

	public abstract Newspaper getParent();

	private NewspaperChapter(String fullName, Class<? extends IArticleFeeder> someClass) {
		this.fullName = fullName;
		try {
			articleFeeder = someClass.newInstance();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		}
	}

	public List<Article> getLatest() throws MalformedURLException, IOException {
		return articleFeeder.getLatestArticles();
	}

	public String getFullName() {
		return fullName;
	}

}
