package com.brothers.pitukh.uanews.model;

import java.util.EnumSet;
import java.util.Set;

public enum Newspaper {

	UP("��������� ������") {
		@Override
		public Set<NewspaperChapter> getChildren() {
			if (chapters==null) {
				chapters = EnumSet.of(NewspaperChapter.UPMainNews);
				//chapters = EnumSet.of(NewspaperChapter.UPPublications);
				chapters.add(NewspaperChapter.UPPublications);
			}
			return chapters;
		}
	};/*, UP2("��������� ������") {
		@Override
		public Set<NewspaperChapter> getChildren() {
			if (chapters==null) {
				chapters = EnumSet.of(NewspaperChapter.UPMainNews);
				//chapters.add(NewspaperChapter.UPPublications);
			}
			return chapters;
		}
	};*/

	String name;
	Set<NewspaperChapter> chapters;
	
	public abstract Set<NewspaperChapter> getChildren();
	
	private Newspaper(String name) {
		this.name = name;
	}
	
}
