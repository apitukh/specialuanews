package com.brothers.pitukh.uanews.model;

import java.net.URL;
import java.util.Date;

/***
 * This is model class for representing Article.
 * @author APitukh
 *
 */
public class Article {

	// Random id for each article
	private long id;
	private String title;
	private String shortDescription;
	private String author;
	private Date date;

	private URL url;
	private String fullText;

	private boolean hasImage = false;
	private URL imageUrl;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public Article() {
		// this.id = UUID.randomUUID().toString();
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortDescription() {
		return shortDescription;
	}

	public void setShortDescription(String shortDescription) {
		this.shortDescription = shortDescription;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public URL getUrl() {
		return url;
	}

	public void setUrl(URL url) {
		this.url = url;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public boolean hasImage() {
		return hasImage;
	}

	public URL getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(URL imageUrl) {
		this.imageUrl = imageUrl;
		this.hasImage = true;
	}
	
	//TODO: override equals and hashcode with Apache
	
}
