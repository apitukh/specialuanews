package com.brothers.pitukh.uanews;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.brothers.pitukh.uanews.model.Article;

public class MySimpleArrayAdapter extends ArrayAdapter<Long> {
	private final Context context;
	private final List<Long> values;
	private final Map<Long, Article> articles;
	
	public Map<Long, Article> getArticles() {
		return articles;
	}
	
	public boolean addArticle(Article article) {
		if (this.articles.put(article.getId(), article) == null) {
			return true;
		}
		return false;
	}

	public MySimpleArrayAdapter(Context context, List<Article> articles, List<Long> list) {
		super(context, R.layout.main_activity_item, list);
		this.articles = new HashMap<Long, Article>();
		for (Article article: articles) {
			addArticle(article);
		}
		this.context = context;
		this.values = list;
	}
	
	private static final SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yy hh:mm");

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(R.layout.main_activity_item, parent,
				false);
		TextView textViewTitle = (TextView) rowView.findViewById(R.id.textView1);
		TextView textViewAuthor = (TextView) rowView.findViewById(R.id.textView2);
		TextView textViewDate = (TextView) rowView.findViewById(R.id.textView3);
		ImageView image = (ImageView) rowView.findViewById(R.id.imageView1);
		
		Article curArticle = articles.get(values.get(position));
		textViewTitle.setText(curArticle.getTitle());
		textViewAuthor.setText(curArticle.getAuthor());
		if (curArticle.getDate()!=null) {
			textViewDate.setText(formatter.format(curArticle.getDate()));
		} else {
			textViewDate.setText("");
		}
		if (curArticle.hasImage()) {
			image.setImageURI(Uri.parse(curArticle.getImageUrl().toExternalForm()));
		} else {
			//image.setVisibility(View.GONE);
		}
		return rowView;
	}
	
	public static List<Long> getIds(List<Article> articles) {
		List<Long> ids = new ArrayList<Long>(articles.size());
		for (int i=0; i<articles.size(); i++) {
			ids.add(articles.get(i).getId());
		}
		return ids;
	}
	
}